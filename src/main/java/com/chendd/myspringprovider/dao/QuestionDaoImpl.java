package com.chendd.myspringprovider.dao;

import com.chendd.myspringprovider.entity.QuestionContent;
import com.chendd.myspringprovider.entity.QuestionInfo;
import com.chendd.myspringprovider.utils.HttpConnectionUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import sun.misc.BASE64Decoder;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Description:
 *
 * @author DongDong.Chen
 * @date 2018-03-13 11:18
 */
@Repository("questionDao")
public class QuestionDaoImpl {
    @Autowired
    private MongoTemplate mongoTemplate;

    private static Pattern pattern = Pattern.compile("<img( ||.*?)src=('|\"|)([^\"|^\']+)('|\"|>| )");

    public List<QuestionInfo> queryQuestion() {

        List<QuestionInfo> questionInfos;
        Query query = new Query();
        query.addCriteria(Criteria.where("content.analyse").regex("base64"));
        questionInfos = mongoTemplate.find(query, QuestionInfo.class);
        for (QuestionInfo questionInfo : questionInfos) {
            QuestionContent content = questionInfo.getContent();
            String contentStr = content.getAnalyse();
            repairContent(content, contentStr);
            System.out.println(content.getContent());

            String id = questionInfo.getId();
            if (StringUtils.isNotBlank(id)) {
                Query q = new Query();
                q.addCriteria(Criteria.where("_id").is(id));
                mongoTemplate.remove(q, QuestionInfo.class);
            }
            mongoTemplate.insert(questionInfo);
        }
        return questionInfos;
    }

    /**
     * @param imgStr base64编码字符串
     * @param path   图片路径-具体到文件
     * @return
     * @Description: 将base64编码字符串转换为图片
     * @Author:
     * @CreateTime:
     */
    public static boolean generateImage(String imgStr, String path) {
        if (imgStr == null) {
            return false;
        }
        BASE64Decoder decoder = new BASE64Decoder();
        try {
            // 解密
            byte[] b = decoder.decodeBuffer(imgStr);
            // 处理数据
            for (int i = 0; i < b.length; ++i) {
                if (b[i] < 0) {
                    b[i] += 256;
                }
            }
            OutputStream out = new FileOutputStream(path);
            out.write(b);
            out.flush();
            out.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static void main(String[] args) {
        String str = "已知与<img class=\"kfformula\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAVIAAABzCAYAAADKUorIAAAOlklEQVR4Xu2dX+g/2RjHd2/9u1IbriRxsxe4IdJHbZYL0iYbkW1FoRVJ8i8X/iXJZltEpIhIEoVF+SbihpIbktwpLnHP867Po6cxM+fM5zkz3zPzeU09/f585pw553XOvOc5zzlz5s47OCAAAQhAIEXgzlRqEkMAAhCAwB0IKZ0AAhCAQJIAQpoESHIIQAACCCl9AAIQgECSAEKaBEhyCEAAAggpfQACEIBAkgBCmgRIcghAAAIIKX0AAhCAQJIAQpoESHIIQAACCCl9AAIQgECSAEKaBEhyCEAAAggpfQACEIBAkgBCmgRIcghAAAIIKX0AAhCAQJIAQpoESHIIQAACCCl9AAIQgECSAEKaBEhyCEAAAggpfQACEIBAkgBCmgRIcghAAAIIKX0AAhCAQJIAQpoESHIIQAACCCl9AAIQgECSAEKaBEhyCEAAAggpfQACEIBAkgBCmgRIcghAAAIIKX0AAhCAQJIAQpoESHIIQAACCCl9AAIQgECSAEKaBEhyCEAAAggpfQACEIBAkgBCmgRIcghAAAIIKX0AAhCAQJIAQpoESPJNCLzZrvKlTa7ERSBwAQGE9AJoJNmUwGvtat8w662vqlz3mD3B7ElnIk+0P/9k9jOzb25K6fYvJh6vMrv/9ouyfQl665zbE+CKvRP4pRXwhZ0J6ZetPH83+4nZTQD4XPv7q83eZ/aI2Tt6h5ss38nSP9Ps+WYPmv3Y7OXJPHeZHCHdZbNdTaE/aDX9yLm2vfTVj1t5/mo2F2r4lv3+GrNPmL3/oK31n7Nw/tn+/K7ZzxHSg7Y01VqNgDyv95qtOYyTt3Pf2eN5mf3Zi5C6h/w6K9PU8F0x3S+aaZj/7NVaoa+MXVjxSPtqF0rTMYEt4pYaPj9q9jGznoRUgqFjztt0PjqvlwfA2t0JIV2bMPkfjsDaQiqPToeGzz/qTEhfYeV5gdl3zH430bLvtP//jNk1xQwR0sPd5lRobQJrCqnCBm83e9O5Er0JaQ1bedOafPmQ2UdrEhzgHIT0AI147VXQBMhzzhD+Zn9qSDzlLelcHZlJkDWFVOWLs+GthFSe5OvNtFRJoYJvm60R4z1Zvpp4UXxUcdSpdvA+6w+OuIzqc/bjD8z02wNmmhnXoYmdn55/8/S9/ImQ9tISlOMiAhKeX4ebS5Mhuondo4uZSky+f/49MwmylpD6sDmKfEZIPe0Y2DWWJ0n4Pmum5VqvrBA81fdtZp80uzkXUu2p5VMvMdOE3mNmD5s5814nsBDSi27fYyQ6WTU0M6wbQJ3fj1/ZX+RJ9Pr093Kq/C81c+FRPX57/nFsksOXE2W9sbWEVEPi4QMgI6Sq7+POPF48aON3nQWqRU8W9+eZvdHsX2ZRGOfy1zIpnRu91iiYWlbkbescENIWLdY4j2uZURxikwC91Uxr/b5i9gczLbD2Q5MJD53/IdH5vNlNY/YtspP3o5vNy6Z/q9xTkxy+vjEbu1tDSCV6fzEbLinKCOmQsc+46//l8WXbNAq18nzGuS9pVFDKW57nP8zkbcYjzvjHMmoCTgvfv2emYX9vBx5pby2ycnl8eKuh3VfNpmJYfp4Xp2aotnLR/y/76MHF8k4tzXEhydaltZCerGbRs44VbSWkkc+aXp3P2Jc8XtVrbM2ljxo0KnrR1h0qcT2ENAFvb0k1BNN7218Y8QTG6hK9g9rJgy2ZqHzuwflMsa4/5m1FIcmORFoLqa8ZHXuotRLS+JaURiFjMeRWbedxzjkxVXuMeZbejmvEcFvVbywfhHRNup3lrQ5+18KbyN9kUVV6fuXPvc2p+GdtfPSPVs9nNWy3Ujw2rhkdu2wrIY0TT9nQRglPjFUvDSE4/7fYRZbueBVDF6Uy1vy+5IGLkNYQPcg5URRrO6rHHYWg1+GWv5KoMk69ulgbHz1ZHk8ptLdigXoHXtcqHfLk55YAjU0wxTxbCGkUNuWdDW2U6qzfvdxLPEux19IpHZq8Ki2dGpZDXq6WUbU4/m2ZLInFIqQtqO8kj/jEro2T+TDNq7jkKb0VFhf7uTq1io+qTi2H9nq4aaZ76tCaTx2aQPPD11nW8vW45ZZt6G2y5OHr5VySppbB2uchpGsT7ij/KKS1nXUPQuqe9tSQtWV8tLWQlrpHC480xo9LoYZSeSR295qVxDz2m9qHbyk+OhVXLZV5i98R0i0od3INDYG11k+H1u/VDF3iTdjru9P+gJiKx9XGR2ubqaVHWrpmCyGND9BsfNTzKvWFS4TUH4hTk1RTM/0lhlv8jpBuQXnH14hx1exNuBYGv7mnPJ/a+Ght+fYkpHHlheo3t/1dTf19Mqi0vMkfwGMesDxL7SYfd9I/2b89PjpWRtXjbrPMq7019bv0HIT0UnJXkC52blX3kgmALTC51zbmkUYhaTXJsichbR2aUexTr+SWPiUyJ7j+W/Rqla/W0mrFxLCdttj/NdtPEdIswQOnjzdhS29UQiSPRG9W6VC8VtuyDd9yqUXrwjZcH6n///D55lRetbG60nW3EtI4237pQywue4rCpVinPgvirwbLc9RbQyWB9Pfp9RmRqVn1UijFhdQFU2WRt/k1M3mlsa+d7N96574Uky212Zq/ezv1uNZ6zXr/L+9WN9Ymhd34InGCptUCbt+kQjPRylM3zo2ZhElbx6kjaqG4z94umRjx3Y2eZuk1C/5PM71yqdcQtTfmkrxKqNcWUnln2vHIZ+y9PD5zX7sL+3DZk9YBy5uUMIm1v24psdJ2dxLVmrXC3o6P2fm/MIuC6iKq9p3ahcuH9k89V0y7Ovn3nTxv/aQ21DKkud28Sm211u/eRvrgX9ynQtfzdor1WqscXeSLkI43Q83rlksbMK71HIuvxbeuJHx+ZNvIY3WlmN6S+qwtpEvKMnduZK7z5OlpK71Pmw0Xu0fRrV1jrAee9mXgK6KtWmyn+WRv0p1We7TYupEUn/INS+RRtNogIq5jnBO04XrH0sxwDX8fRi59w2Yubxf9zFZ8NWXPnhNfpvC85uLE8fxLQwnZMpN+hwSuXUjjMMqb7+n2F20srB2hlr6iN9YFondbM7yOS3VKw8yTXfANZr+ZKOtU7HSHXfWiIscVF8qg5GnWPvAuKgyJjkvg2oV0qmU9ZvlkO6F2g5OxvNxz83fXa7zCODlSWqrjQjHluXpe1+hdDeOjNXHuuMKh5qF3XGWgZosIIKTTuHx2VoF0CdUHzJa++xxn/WtuZJUmeqQlAfRzxzwt965aLXla1LE6OHkYH63hEIW09s23DqpKEW6bAEI63wLRq9GNNbfkZZiTht2+wLpmWKlzlt7ImkgaxnFVZi3ruc/sPWY1b2/ddj9c4/ox3lm7r8JtvJO/Rt3Jc2MCCGkZeLwhlwz3hhMdNayjB1uza5BE8wGzx5v5Uhp9/E7x3UvXpJaJ7OOMGB+tYalatV68vw9SlDJNoObmTl9k5xkMd8ovxS29unGIXjus91c5lUfL5Uo7b4LFxR/GR0uTTH6BGJ+u9WIXF44ExyOAkNa16VJRHMbnakWx9TeF6mp3vLOG/Gv7eeTfYunZ8chSo1ECtR3sKPhO54rcLKxQ9FRqJiGGQ8Sa2fqh53ttbbOwSWZPj/xrwzHqGzGmXVp61rK85LVzAtd0s2a+2TP8PnqJ2yVDxFi+2pt/591vteJf8lmR4URTbThgtUqQ8X4IlARhPzUpl3T4PZua5TBjsTP9X4lb/O5R7RAx7nvacoOUMpnjnRHburadI/+aNj4eNWp0MYGSIFyccYcJh0JaM9z2asQZ4BphXPJ2kl8jiu/YNmp6n3tpSKLDZtikSJF/bR+PaWpn+TepDBfpn0BtJ+u/JuUSZjZoXjrZFM+vmWg6WfFjfG7YLlpKpV2Aet3Ut0x/2zNqd7H3Ug0np5Y8ZLetGVfrksA1CamGbjqWfs98OAlUEzuLol2zXCrG58Y8XuWnrdRKe2V22cluoVClV2eHRYrDeiaZbqHB9n7JaxJSf2toqbcRZ4Br1xbG9aA1QhonR4Y3snur19RW2fvK+deEYeJoYOnba9lykv4gBK7t5tQNplhj7cbAauYYu6zxRpVmyS5Cpdli3/Py/oP0uS2qEVdAlPp4fOjVTkxtUQeusSMCpU62o6pUFdU3IvGd6EuJlr6y6fnFN2vmJi4UNviU2e/N/LMjQw9Ww9TMZ0hKdTzi75H/nDgueeAdkRN1akTg2oRU2DSU02cldMzFHTPrTpV3TD+2i5MmON5tpo1F9GkQn2yKXq9/c6n3DZQbdcem2Tj/qeF9FNHakUbTApLZcQhco5Cq9Xyzj4fs74qL6bs72uhDx11m95r594Iyazrdo9U1dGPfmPn3erQ9X9ydyTc58cX4PsnFTX75/RY/ifx1y0Y7YZ3MtDOW2l4i2/NH5S6vOSk3JXCtQuqQ/aYafmhNN5jE74dmS/cgHTbg2BdDlb97xfH8+GVLhR/Gvi20aQc5wMXE/x6zB0Nd9LDSN+VbfAHhAIioQpbAtQtplh/pIQABCBRfdQQRBCAAAQgUCOCR0kUgAAEIJAkgpEmAJIcABCCAkNIHIAABCCQJIKRJgCSHAAQggJDSByAAAQgkCSCkSYAkhwAEIICQ0gcgAAEIJAkgpEmAJIcABCCAkNIHIAABCCQJIKRJgCSHAAQggJDSByAAAQgkCSCkSYAkhwAEIICQ0gcgAAEIJAkgpEmAJIcABCCAkNIHIAABCCQJIKRJgCSHAAQggJDSByAAAQgkCSCkSYAkhwAEIICQ0gcgAAEIJAkgpEmAJIcABCCAkNIHIAABCCQJIKRJgCSHAAQggJDSByAAAQgkCSCkSYAkhwAEIICQ0gcgAAEIJAkgpEmAJIcABCCAkNIHIAABCCQJIKRJgCSHAAQggJDSByAAAQgkCSCkSYAkhwAEIICQ0gcgAAEIJAkgpEmAJIcABCCAkNIHIAABCCQJIKRJgCSHAAQggJDSByAAAQgkCSCkSYAkhwAEIPBfhzGqkjYPU+YAAAAASUVORK5CYII=\" data-latex=\"3{a}^{y+4}{b}^{3x-1}\" width=\"78\" height=\"33\" style=\"width: 78px; height: 33px;\"/>是<img class=\"kfformula\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAY4AAABzCAYAAACPf+UPAAAPJklEQVR4Xu2dX+i9yRzHd2/92Su1xZWLzd64wA2RvkrYC9o2EZFtNwqtSJJ/7QWRJCJLRMpGJInyd8s3ETeU3JDcbnFp3fN51/PZPj09fz5zzpxzZs68Tk2/8/ueeeaZec088575fGbmufMOPhCAAAQgAIECAncWxCUqBCAAAQhA4A6Eg0YAAQhAAAJFBBCOIlxEhgAEIAABhIM2AAEIQAACRQQQjiJcRIYABCAAAYSDNgABCEAAAkUEEI4iXESGAAQgAAGEgzYAAQhAAAJFBBCOIlxEhgAEIAABhIM2AAEIQAACRQQQjiJcRIYABCAAAYSDNgABCEAAAkUEEI4iXESGAAQgAAGEgzYAAQhAAAJFBBCOIlxEhgAEIAABhIM2AAEIQAACRQQQjiJcRIYABCAAAYSDNgABCEAAAkUEEI4iXESGAAQgAAGEgzYAAQhAAAJFBBCOIlxEhgAEIAABhIM2AAEIQAACRQQQjiJcRIYABCAAAYSDNgABCEAAAkUEEI4iXESGAAQgAAGEgzYAAQhAAAJFBBCOIlxEhgAEIAABhIM2AAEIQAACRQQQjiJcRIYABCAAAYSDNgABCEAAAkUEEI4iXESGAAQgAAGEgzYAAQhAAAJFBBCOIlxEhgAEIAABhIM2cEkC77ebv3DKwAvs36cs/MPCHyx875IZO+O9e2TwFuNzv4U3n5HTIbfqke0h5Tz7NQjH2ZFzQyPwYgsftvCEhW8EIjf2/d0W3mThBxY+a+HPV0qsNwaqm3ssvNTCQxZ+YeG+RuumN7aNYlzPFsLRXZVdRYa/b6V43MJPV0rzzQ46p2MroicG/5uEQrPBH1n4TePC0RPbY9vRRa5HOC6CffibyoTwxR0Kf7PfZb76QCJuj0B7ZuBC0uqMo2e2XbRlhKOLahoyk5+2Un/EgkxWrdvST1VBrTJoXTgy9dEq20zeLx4H4bh4FQyZAXU877IQ/RtzEHLAfnf64zW2054ZtC4cPbPtokO4xgeyC/CDZ1IP9t5MYgTh6JVBD8LRK9suugaEo4tqurpM7jkvVWDZqb9g4fcWXrFBQCto3mvhWRbumuI9Zv/K8a7fHrSg1UD6yLn76+m3S0OtyWCtLK+3H942cXldQqyzTFoXjlpsZc560QTlSfv3KxbWVvkprj4fzULsOR7C0XPtXXfefWXVl62Y71spqjrG91jQst3bKY7brl9l/9eS319akCPeZzB/t+/3doIuw2BelJ/bHyQSS58tliVIWheOTFn22KodaT+Rr/z7nX1X23l4IXG1w59Mv/fStjKMVuMgHEfhu9jFN3bnByxoRP3ykAuNzjUiamVUfSggletP08USgNuVhDSynO/1iAKhpaM+AvQOtRfhyDKYo/m4/eEZ0x9fOWsftVao9S4ce2z1fL0mtJ0Yf6nPFPNPWtgzjx36PDR3HcLRXJVsZkgN2jfIfcu+/9XCv8IVL7Pvj0z/VyP+6kan23LJvzSV4zPh4Z3nVyPCf1uYL+uNvpEoOu+0uNq89mMLa/tHWmKSYZDJrzp5/2yJcCYtj9O7cOyx1e8adNxOBfb4a5seNYDRptVPWPhUCche4yIc/dScT4dlbvi2hTVbq8fzkr2hk47S8+sdv4RxySzg8TSDWNpH4KO/Pd9IyzWfZbBXhtgWas60ehaODFuZsbztRYZrAxkX596etb32s/o7wnEwurNeqKmylqZ+zcLexjllLI661WG8dUNozlqQnZvd2O/albwnGkpGD/TSzGHPdt1SeZfyUsJgrywuooqXYbqXnv/eq3Bk2er58bPSvD2p7Esztigsw/SnwxQ0+0Q0Gk9mmbstbI3A51mXM8/9H1smn1aKLHGUSWDNAZnNp+8439snspReNOtk77cVr/T5qsXA8xQd5TXNKIcIR69sPd9r/ovh/BtqXKUNu8bDRBrlBKIIZDtEt8vqbq2bbWp1mD6iVJlfYqH0gESNHrWst8bnv5ZIiS+lFgPPe3To6m81zSiHCEePbOUX+/oEVLP2pRObh/NvIBw1uofzpBFHa1lbtS9L9Ry2PEjQw6eljxkz3Bbx7N6P89Ra2V1qMfC7OotT1P8hwlFGo27sQ9n64GvrmRvOv4Fw1G2cp0wtCkd29tCLcMiGrNVha6JxY7/dJuHu+TfW/CLJ5E8WrSYDz2S0zddeJtqTcBzD1mf6a2a+If0bCMfJ+oHqCWvK/I4pVe1byJhAYsfR6rsT9h5qFVk25OwSR3/Q1/YrrK3Eql5hBQnWZuC3joONmv4Npd+LcBzL1hmuLWMe0r+BcBQ83R1GjX6R2h1HDRx66OQH2DNPyVww3zmukZ7eQKcXQbndWTMTrcjSZ8kerZUyettgS0dCHMNgqw7iqro1HsfUYQ/CUYOtC8eamXdI/wbCccyj0/a1sRNVTg9xFJ+yhLK/67MnGhIIbWqcd/a+cirOpCQw2u2rd3jMHcH+RriWjmc/lsFW/ZzaTNm6cNRi66vSlmYcUZxrLjw45XNXLe2WHabVCjlgQrHjqDnb8HdNa5esPvK3/DAhAPMqKFmauZR/Fw5/YP3d0t+xG2nWEa+RiOrMKj/4sJXmcCyDrXLEZbhRXMXpjRZ8mbZ8H9pJX/J+d1+t1fL+oFps1zYL6u+PToMU1cNw/ehwBW6l1zhhPqLDrtamLz9MUIfnKU110LcW9ADpZFrfe+ErefacsSUP9pLZyU1Vz5046tRbN2d5XvXTfyzIHLZ1qukJq2Iz6WMZrCU+X4arPTxasSbxVD35kSs39l2+I4nI3j4fzeZ0wvCzp/jx3hImfWIdXIqp37cmWz9h+HmW+FNTm/qn/avjbnR6815bvzSLk9wf4TgJ1oslmjkeoTRzcS37ktM57mrXg+Qf2lYp6TrxY30pRc2+dLT65y3MX5wVRSa7P6hOLvtPxRef1Do4sisiPNxdVddiZvXwy67vBxxqRlDrIL+4F2DrAZnvGWh1FVf/tb1fgrjx02Nv2eBj/NZ8YfulvVwMN5fWOjjyciU54M4IxwHQGrgkmmM8O8+3L7+yoD0RW69kzWY/zl4y0/FoHtgzfWTzQLxyAnE1na7em0lkBwflOen3ihvL+tst/HHlWcoclNhv6RM5RzgSkDqK4j6H51ieswciLhXPzU+ayeiTGVVFh+za8Qwdoewyq3P/RsbHFVcHZQYIXYIpzLSL79rM2dv6sDM0hKOwRXUQ3c88ktNTDf9jFkrPbIqrsjKdj7DEGcewD9SF28fcv5FZJhqFI3sqwYWLefLbe1temq35DC3D9uQZvdQNEI5LkT/tfePIU52BVhxlxePG4vpGOuVyz9ShOHQ+p63PbOrRX5E90+yUZ1pl891aPDm+535CPVNayvyAhQ9ZyJze0Fq5quVnZOGYj86qQd1J6FyjutiJlJgg5s7VTBuJM5Ra77U+V31c032ifyNbD6feLNgjX4nEgxaeacGXfD9p37fOVOuxnAfnOdMpHJx44xdeu3DM3wSY9TtEk1PWTOVHL6jKh1ye2EBbn/s3MjNFZTv6prKzlAaKSxYuSWBk4bgk93Pdu1QE5mKaFYF4n4wj/VzlH+k+87rLPtux7lhGPVKLOaKs2cZ1xC24tAKBmymN28K04mgyYyKbmy0yIjCf2dCmCiupUvRYd1nTpNpV9GexjLpSZVx7Mjzk7dfwMe+NjsKhku7V9yFmi5i/bIfVPvX+cnjIa2LnjvGseas/OuS4KoG9jqTqzUjsIALzc3dKlgGWCofvhlVGs2aL+N6PmgcqHgRr4ItiO8m2kVh3mYHFwHgpeiSAcLTfHubCkTEfeaniKpuMEByy+zuKzdJx5ndZZkpNbO3XSns5jHWXfa7jNdlVWO2VnBydnUC2gZ09Y9zwaQLHvJCp1Dke42cc4zeWy2gjn7cnLe3V6bQtvTzpWpuW111mgCAGc2d6yYDkWhlSriQBhCMJ6oLRZE7Q5+HCPMyd1hn7dRSpzPLdaCNf6rCUno40L3nfQ2ExiT4R2DsmYw4qmqlwitOMigggHEW4LhLZd2WXjgjjKpvs+vy4HyMjHNGHMu98fDZCGztPs/G6y8w44kyx9GSB85SGuzRNgIe66ep5OnPqFOQruK8gu9H3kJltKOmSk1L3VuTodx313tLrWgvwdRc1rm7be67jACHrSO8OCBk+HYG9Bna6O5NyCQE/uNDftLd37aFHgMTdx1vOUpnBPmfhLxb8NbLzGYpMJ4e8VnavbPy+TCDW3ZYYlAwOYA2BRQIIRz8N48ayqld96rPlNzhm34fSjtcvnXIrp+oHLeigN71K053jcVbj72S+tx+8V5FTr7s1c1UUjews9CrAUIi6BBCOujxPnZofvvaI3Ui26d9a0MFr+txt4bUW9F5wfY7ZU+EzFt1DndGtBX/Pt45rj6eD+qGIvvnPnfJ0TKduDcvpu9Nb9fG4BZ3iemNBp7qq3UhUHpv+fpkcctfuCSAcfVahdwT3BKFQSdQpqLP/mYXsMeprBDRruN+Cm6KUrtL3WU+8TiNZHTktUZE5ben91n2S7jPXqrtXW3goZF9C8oSFGm+H7JMKua5GAOGohpKEIAABCIxBAOEYo54pJQQgAIFqBBCOaihJCAIQgMAYBBCOMeqZUkIAAhCoRgDhqIaShCAAAQiMQQDhGKOeKSUEIACBagQQjmooSQgCEIDAGAQQjjHqmVJCAAIQqEYA4aiGkoQgAAEIjEEA4RijniklBCAAgWoEEI5qKEkIAhCAwBgEEI4x6plSQgACEKhGAOGohpKEIAABCIxBAOEYo54pJQQgAIFqBBCOaihJCAIQgMAYBBCOMeqZUkIAAhCoRgDhqIaShCAAAQiMQQDhGKOeKSUEIACBagQQjmooSQgCEIDAGAQQjjHqmVJCAAIQqEYA4aiGkoQgAAEIjEEA4RijniklBCAAgWoEEI5qKEkIAhCAwBgEEI4x6plSQgACEKhGAOGohpKEIAABCIxBAOEYo54pJQQgAIFqBBCOaihJCAIQgMAYBBCOMeqZUkIAAhCoRgDhqIaShCAAAQiMQQDhGKOeKSUEIACBagQQjmooSQgCEIDAGAQQjjHqmVJCAAIQqEYA4aiGkoQgAAEIjEEA4RijniklBCAAgWoEEI5qKEkIAhCAwBgEEI4x6plSQgACEKhGAOGohpKEIAABCIxBAOEYo54pJQQgAIFqBP4Pz8DXkgkgaG8AAAAASUVORK5CYII=\" data-latex=\"-3{a}^{2x-2}{b}^{1-2y}\" width=\"72\" height=\"31\" style=\"width: 72px; height: 31px;\"/>同类项，则<img class=\"kfformula\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQUAAABzCAYAAACRmhf5AAAKIklEQVR4Xu2dTa8tQxSG3V9hbCBMDDASN3IkEh8JERFCgoQQHyEMRHyNfMXoChMh5AohDEQYECTOgBgxMSHiD5hizHpjl9Rpu3ev3ru7V1XXs5OVc+85tWtVPavq7arq6upT5/CBAAQgkBE4BQ0IQAACOQFEgfYAAQicIIAo0CAgAAFEgTYAAQj0E2CkQOuAAAQYKdAGIAABRgq0AQhAwEmA6YMTFMkg0AoBRKGVSFNPCDgJIApOUCSDQCsEEIVWIk09IeAkgCg4QZEMAq0QQBRaiTT1hICTAKLgBEUyCLRCAFFoJdLUEwJOAoiCExTJINAKAUShlUhTTwg4CSAKTlAkg0ArBBCFViJNPSHgJIAoOEGRDAKtEEAUWok09YSAkwCi4ARFMgi0QgBRaCXS1BMCTgKIghMUySDQCgFEoZVIU08IOAkgCk5QJINAKwQQhVYiTT0h4CSAKDhBkQwCrRBAFFqJNPWEgJMAouAERTIItEIAUWgl0tQTAk4CiIITFMkg0AoBRKGVSFNPCDgJIApOUCRbLYFHrWYXbWp3gf38w+xXs+/NPlhtrXdUDFFoMerUWQQuMXvC7GuzNzMkR/bvB8xuMfvI7GWzH1tChii0FG3qmhP40P7zntlnPVjest/fbfaF2bUtoUMUWoo2dc0JaNrwygCSn+3vmlI85ki7GrqIwmpCSUVmIPCi5fnkZhpx6wz5F5klolBkWCjUAgT+Nh/3meXrCV23t9kv3t/8spm+0kxFF2hkuKiLgERBC4m7RgCIQl0xpbQQOIjA0EKjMte6wxmz78xOH+Stoi8zUqgoWBR1cQLpDsRr5vmRxb0HOUQUgsDjtngC2sfww6aUV9rP4+JLPFEBEYWJQJLN6gi8ajV62Owls6dWV7sdFUIUWoo2dfUSSAuMb9sX7vF+aS3pEIW1RJJ6TEXgyDL6xqxJQRBERGGqpkQ+ayCgdQRNG35pcYSQAogorKEpz1OHZyzb8xrqHAjCph0hCvN0qDXk+rlV4pqGRpPat6DHpYeeh1hDbHfWAVFYfYj3rmBLoqD9CD/tEAStMxzvTbKyLyIKlQVsweK2IgpDgiDkmko9vyD7UFeIQij+op23IArq7H86pgxafGRHY9HNlcItQWDtoqDnGvQZWkO43tJcZtbMBiZGCv8ODbWgdvmmkeiknY/Ntj1Sq+frr8jS6im7T8w8Z/mpcT248SVXug/+rtmxs4drdfw6s9+c/pzZ9iaLFoW5eekpSe/nWUvI9MFLq+J0anQ6o08HdSYRUMd7yEzHcOWbV9LtKp3Vd9ZMP+81u8tMYuJ5Ll/56rw/Hf+VP5Lr3Vf/7caXkC8h5pGiID5z8xojCrcvJMRFdKclGlcRFe0UQoLwqdm2K4AEQAdr6Biu9HfdrtLqdH61SKIiUdBmlwt7KpryU8PKDwBNnc6zcy6VN7lYIm5RolArrxLb+V5lWqJx7VWwGb+UrvqaJvQNCdPDMOrsr5tdbdY9vLN7peljqSnH72bduWs6/89z5dcU57kNk6UOEo0ShVp5zdhkl826RVFQhz9/SyfPyaez+dLvutODfPivNH0dte+qN/bKr5GKjhzXZ6mn9iJEoWZey/bcGb21JgpHxlIPuwzN43NR6JsapE4jQXjabNu7AbTCrZXr7pFf6fAOhXboSDClyUclS81vI0ShZl4zdtNls25NFNQZ9Rl6HDZ1CKX1zPn7orZtLSIJU/rO0PHhY0cVU7WgCFGomddU3MPzaU0UPCf4dq/MQ512VxDl7waz/IUj+ShB3720Z5SR8o1YT5DvCFGomVd4Z56qAK2JgtYChvYUdK/MQ1ONXbFQXt03EOVTAc/UIWI9IUoUauY1VZ8Mz6c1UfAAz6/Mu241evLqptHehjeyXw7tb+iOWsauJ6TTiPcp66HfOWTalXwvzevQOq/i+4jC/8OYX5mnaNi5h+7UYYj/oesJGhnduWdL1S5PfbSQus9Hx6IfugtwaV771HN13xlqlKursKNC+fD+kPWEba7yvD3HhketJ6jsEWsKXWY18XI0rTqSIAon49Tdf3DIekK3BXSv+t0FyG0tJmo9oQRRqI1XHT3eUUpE4SQkz/4EB9atSfbJO79SetYf9i3btu9FjxRq4zUl+9C8EIWT+MfuT/DczUge8rw9U4fulXLo1uXUDSlaFGrjNTX/sPwQhZPox6wnpKmGl2H+rIPnqp+vJ0S8yzBaFGrjFdaJp3bsbdBT+y0xv+7tr6E5v3d3ZKprLjieq36+8u4ZWUzNNFoUauM1Nf+w/FoSBT1s84KZbrVt2zQ05vbXkeXheYYiD2zeyD3cx14pp25EJYlCDbym5h+Wnwd2WOEmdpzPUZV1PhLIXyaa3O5iM3aUoDzHHJLSvQviGVlMjCv8lmRtvKbmH5ZfS6KQX6m7OxXTSrf2JZzZRKOvI6ZdgmM7aj4S2TU16QpUxHqCEESPFGrjFdaJp3bckiik4Xj3bMS0yp82KqUr1LaNS2ndYWi9YVuc0pRDf+t75kFptAswnReptBHrCSWIQm28pu6bYfm1JArpTMV3jHY6lDVd9fPtzPmtwPSsgRroTWZ6Nfk+gpACnE8L1Nl1NuSxmfI/babTlSRG95vpODh9PHcq5mhA0SMF1akmXnPEICTPlkQhNbIb7R/pFCMNzXORSEFQJ73DTFdsdU5NN740O2u27TCVMcHT9OBms4vN0vMFyl9l0enO+mgRM33GTlPGlGVX2hJEQeWrhddU3MPzaU0UwoE7ChD5vENevFJEYQhZKbyGylnN3xGF8kKVr7pP/UDWmNrWIgql8BrDtui0iML84dHwV++X0JRl6CTmfA49lHbukqss55oNvUFp6nLUymtqDmH5IQrzo+9uitp1UEp+1TtkQXP+Ws3nAV7zsXXljCi4MB2UqPt+iL6Fw/ypwMhpw0GVneDL8JoA4iFZIAqH0PN9NzVy3WF43Kx7ZqNyyY9Na1kQxAJevnY1WypEYTa0/2Ws4fBfZn2vMkcQTsYAXvO3yZ0eEIX5A5BeU6eRgt5QnUYK2kx1lVlagNTLZ4/nL07xHuAVHCJEYZkApA04+WvstVlJG6G+6plSLFOyMr3AKzAuiEIgfFxDoEQCiEKJUaFMEAgkgCgEwsc1BEokgCiUGBXKBIFAAohCIHxcQ6BEAohCiVGhTBAIJIAoBMLHNQRKJIAolBgVygSBQAKIQiB8XEOgRAKIQolRoUwQCCSAKATCxzUESiSAKJQYFcoEgUACiEIgfFxDoEQCiEKJUaFMEAgkgCgEwsc1BEokgCiUGBXKBIFAAohCIHxcQ6BEAohCiVGhTBAIJIAoBMLHNQRKJIAolBgVygSBQAKIQiB8XEOgRAKIQolRoUwQCCSAKATCxzUESiSAKJQYFcoEgUACiEIgfFxDoEQCiEKJUaFMEAgkgCgEwsc1BEokgCiUGBXKBIFAAohCIHxcQ6BEAv8APgDdg4vnZ/0AAAAASUVORK5CYII=\" data-latex=\"xy+{y}^{2}\" width=\"61\" height=\"28\" style=\"width: 61px; height: 28px;\"/>＝<div id=\"35001ed8-0a4c-403e-a7fa-bfe2b59dce8a\" name=\"35001ed8-0a4c-403e-a7fa-bfe2b59dce8a\" class=\"FEBox\" itype=\"otherinput\"></div>&nbsp;。";
        repairContent(new QuestionContent(), str);
    }

    public static void repairContent(QuestionContent dto, String content) {
        Matcher matcher = pattern.matcher(content);
        String result = content;
        while (matcher.find()) {

            String group = matcher.group();
            if (group.indexOf("base64") <= 0) {
                continue;
            }
            String substring = group.substring(group.indexOf("src") + 5, group.length() - 1);
            String fileName = UUID.randomUUID().toString() + ((substring.indexOf("image/jpg") > 0) ? ".jpg" : ".png");
            String replaceStr = "/JX_System/2019-4-14/" + fileName;
            result = result.replace(substring, replaceStr);

            dto.setAnalyse(result);

            substring = substring.replaceAll("data:image/png;base64,", "");
            substring = substring.replaceAll("data:image/jpg;base64,", "");
            String file = "D:/tikutupian/" + fileName;
            if (!generateImage(substring, file)) {
                System.err.println(substring);
            }
        }
    }
}
