package com.chendd.myspringprovider.retention;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Description:
 *
 * @author DongDong.Chen
 * @date 2018-04-21 13:58
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface MyRetention {
    String domainString() default "$yimi-yts-upload-oss$";
}
