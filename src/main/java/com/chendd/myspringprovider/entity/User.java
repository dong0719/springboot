package com.chendd.myspringprovider.entity;

import javax.persistence.Id;

/**
 * Description:
 *
 * @author DongDong.Chen
 * @date 2017-11-22 17:42
 */
public class User {
    @Id
    private Integer id;
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
