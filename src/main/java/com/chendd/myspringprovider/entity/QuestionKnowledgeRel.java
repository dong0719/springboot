package com.chendd.myspringprovider.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "question_knowledge_rel")
public class QuestionKnowledgeRel {
    @Id
    @Column(name = "KonwledgeId")
    private String konwledgeid;

    @Id
    @Column(name = "QuestionId")
    private String questionid;

    private String type;

    @Column(name = "OrderNo")
    private Integer orderno;

    @Column(name = "IsVisible")
    private Boolean isvisible;

    /**
     * @return KonwledgeId
     */
    public String getKonwledgeid() {
        return konwledgeid;
    }

    /**
     * @param konwledgeid
     */
    public void setKonwledgeid(String konwledgeid) {
        this.konwledgeid = konwledgeid;
    }

    /**
     * @return QuestionId
     */
    public String getQuestionid() {
        return questionid;
    }

    /**
     * @param questionid
     */
    public void setQuestionid(String questionid) {
        this.questionid = questionid;
    }

    /**
     * @return type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return OrderNo
     */
    public Integer getOrderno() {
        return orderno;
    }

    /**
     * @param orderno
     */
    public void setOrderno(Integer orderno) {
        this.orderno = orderno;
    }

    /**
     * @return IsVisible
     */
    public Boolean getIsvisible() {
        return isvisible;
    }

    /**
     * @param isvisible
     */
    public void setIsvisible(Boolean isvisible) {
        this.isvisible = isvisible;
    }
}