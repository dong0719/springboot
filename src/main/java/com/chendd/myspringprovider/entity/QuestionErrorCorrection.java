package com.chendd.myspringprovider.entity;

public class QuestionErrorCorrection {
    /**
     * ID
     */
    private Integer eId;

    /**
     * 试题ID
     */
    private String quesitonId;

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 用户名称
     */
    private String userName;

    /**
     * 纠错内容
     */
    private String content;

    public Integer geteId() {
        return eId;
    }

    public void seteId(Integer eId) {
        this.eId = eId;
    }

    public String getQuesitonId() {
        return quesitonId;
    }

    public void setQuesitonId(String quesitonId) {
        this.quesitonId = quesitonId == null ? null : quesitonId.trim();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }
}