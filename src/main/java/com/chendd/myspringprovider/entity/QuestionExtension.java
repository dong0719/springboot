package com.chendd.myspringprovider.entity;

import com.yimi.yts.common.base.BasePojo;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "question_extension")
public class QuestionExtension extends BasePojo {
    @Id
    @Column(name = "QuestionId")
    private String questionid;

    /**
     * 得分率
     */
    @Column(name = "CorrectRate")
    private Double correctrate;

    /**
     * 答题次数
     */
    @Column(name = "answer_number")
    private Integer answerNumber;

    /**
     * @return QuestionId
     */
    public String getQuestionid() {
        return questionid;
    }

    /**
     * @param questionid
     */
    public void setQuestionid(String questionid) {
        this.questionid = questionid;
    }

    /**
     * 获取得分率
     *
     * @return CorrectRate - 得分率
     */
    public Double getCorrectrate() {
        return correctrate;
    }

    /**
     * 设置得分率
     *
     * @param correctrate 得分率
     */
    public void setCorrectrate(Double correctrate) {
        this.correctrate = correctrate;
    }

    /**
     * 获取答题次数
     *
     * @return answer_number - 答题次数
     */
    public Integer getAnswerNumber() {
        return answerNumber;
    }

    /**
     * 设置答题次数
     *
     * @param answerNumber 答题次数
     */
    public void setAnswerNumber(Integer answerNumber) {
        this.answerNumber = answerNumber;
    }
}