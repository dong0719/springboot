package com.chendd.myspringprovider.entity;

import com.yimi.yts.common.base.BasePojo;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "question_content")
public class QuestionContent extends BasePojo {
    @Column(name = "QuestionId")
    @Id
    private String questionid;

    @Column(name = "UIUrl")
    private String uiurl;

    @Column(name = "Analyse")
    private String analyse;

    @Column(name = "Reply")
    private String reply;

    @Column(name = "Appraise")
    private String appraise;

    @Column(name = "RightAnswer")
    private String rightanswer;

    @Column(name = "UISource")
    private String uisource;

    @Column(name = "Content")
    private String content;

    public String getQuestionid() {
        return questionid;
    }

    public void setQuestionid(String questionid) {
        this.questionid = questionid == null ? null : questionid.trim();
    }

    public String getUiurl() {
        return uiurl;
    }

    public void setUiurl(String uiurl) {
        this.uiurl = uiurl == null ? null : uiurl.trim();
    }

    public String getAnalyse() {
        return analyse;
    }

    public void setAnalyse(String analyse) {
        this.analyse = analyse == null ? null : analyse.trim();
    }

    public String getReply() {
        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply == null ? null : reply.trim();
    }

    public String getAppraise() {
        return appraise;
    }

    public void setAppraise(String appraise) {
        this.appraise = appraise == null ? null : appraise.trim();
    }

    public String getRightanswer() {
        return rightanswer;
    }

    public void setRightanswer(String rightanswer) {
        this.rightanswer = rightanswer == null ? null : rightanswer.trim();
    }

    public String getUisource() {
        return uisource;
    }

    public void setUisource(String uisource) {
        this.uisource = uisource == null ? null : uisource.trim();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }
}