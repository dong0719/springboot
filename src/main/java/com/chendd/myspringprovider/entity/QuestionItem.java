package com.chendd.myspringprovider.entity;

import com.yimi.yts.common.base.BasePojo;

import javax.persistence.Column;
import javax.persistence.Table;

@Table(name = "question_item")
public class QuestionItem extends BasePojo {
    @Column(name = "QItemId")
    private String qitemid;

    @Column(name = "QuestionId")
    private String questionid;

    @Column(name = "OrderNo")
    private Integer orderno;

    @Column(name = "IsRight")
    private Boolean isright;

    @Column(name = "Score")
    private Double score;

    @Column(name = "Contents")
    private String contents;

    /**
     * @return QItemId
     */
    public String getQitemid() {
        return qitemid;
    }

    /**
     * @param qitemid
     */
    public void setQitemid(String qitemid) {
        this.qitemid = qitemid;
    }

    /**
     * @return QuestionId
     */
    public String getQuestionid() {
        return questionid;
    }

    /**
     * @param questionid
     */
    public void setQuestionid(String questionid) {
        this.questionid = questionid;
    }

    /**
     * @return OrderNo
     */
    public Integer getOrderno() {
        return orderno;
    }

    /**
     * @param orderno
     */
    public void setOrderno(Integer orderno) {
        this.orderno = orderno;
    }

    /**
     * @return IsRight
     */
    public Boolean getIsright() {
        return isright;
    }

    /**
     * @param isright
     */
    public void setIsright(Boolean isright) {
        this.isright = isright;
    }

    /**
     * @return Score
     */
    public Double getScore() {
        return score;
    }

    /**
     * @param score
     */
    public void setScore(Double score) {
        this.score = score;
    }

    /**
     * @return Contents
     */
    public String getContents() {
        return contents;
    }

    /**
     * @param contents
     */
    public void setContents(String contents) {
        this.contents = contents;
    }
}