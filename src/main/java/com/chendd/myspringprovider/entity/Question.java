package com.chendd.myspringprovider.entity;

import com.yimi.yts.common.base.BasePojo;

import javax.persistence.Column;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;

public class Question extends BasePojo {
    @Id
    @Column(name = "QuestionId")
    private String questionid;

    @Column(name = "Subject")
    private String subject;

    @Column(name = "Grade")
    private String grade;

    @Column(name = "IsEnabled")
    private Boolean isenabled;

    @Column(name = "QType")
    private Integer qtype;

    @Column(name = "ParentId")
    private String parentid;

    @Column(name = "HasChild")
    private Boolean haschild;

    @Column(name = "ModelId")
    private String modelid;

    @Column(name = "Score")
    private Double score;

    @Column(name = "Difficulty")
    private Integer difficulty;

    @Column(name = "Virtual")
    private Integer virtual;

    @Column(name = "QuestionCt")
    private Integer questionct;

    private String froms;

    @Column(name = "CreateTime")
    private Date createTime;

    @Column(name = "OrderNo")
    private Integer orderno;

    @Column(name = "IsComplex")
    private Boolean iscomplex;

    @Column(name = "City")
    private String city;

    @Column(name = "Remark")
    private String remark;

    @Column(name = "Editor")
    private String editor;

    @Column(name = "OrigiYear")
    private Integer origiyear;

    @Column(name = "AuditStatus")
    private Integer auditstatus;

    @Column(name = "AuditUser")
    private String audituser;

    @Column(name = "AuditTime")
    private Date audittime;

    private Integer questionnum;

    @Column(name = "UseCount")
    private Integer usecount;

    @Column(name = "CorrectRate")
    private BigDecimal correctrate;

    @Column(name = "Class")
    private String className;

    /**
     * 收藏次数
     */
    @Column(name = "collect_number")
    private Integer collectNumber;

    /**
     * 是否自动批改(0否1是)
     */
    @Column(name = "auto_correcting")
    private Boolean autoCorrecting;

    @Column(name = "Answer")
    private String answer;

    @Column(name = "source")
    private String source;

    @Column(name = "knowledge_count")
    private Integer knowledgeCount;

    public String getQuestionid() {
        return questionid;
    }

    public void setQuestionid(String questionid) {
        this.questionid = questionid;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public Boolean getIsenabled() {
        return isenabled;
    }

    public void setIsenabled(Boolean isenabled) {
        this.isenabled = isenabled;
    }

    public Integer getQtype() {
        return qtype;
    }

    public void setQtype(Integer qtype) {
        this.qtype = qtype;
    }

    public String getParentid() {
        return parentid;
    }

    public void setParentid(String parentid) {
        this.parentid = parentid;
    }

    public Boolean getHaschild() {
        return haschild;
    }

    public void setHaschild(Boolean haschild) {
        this.haschild = haschild;
    }

    public String getModelid() {
        return modelid;
    }

    public void setModelid(String modelid) {
        this.modelid = modelid;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public Integer getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Integer difficulty) {
        this.difficulty = difficulty;
    }

    public Integer getVirtual() {
        return virtual;
    }

    public void setVirtual(Integer virtual) {
        this.virtual = virtual;
    }

    public Integer getQuestionct() {
        return questionct;
    }

    public void setQuestionct(Integer questionct) {
        this.questionct = questionct;
    }

    public String getFroms() {
        return froms;
    }

    public void setFroms(String froms) {
        this.froms = froms;
    }

    public Integer getOrderno() {
        return orderno;
    }

    public void setOrderno(Integer orderno) {
        this.orderno = orderno;
    }

    public Boolean getIscomplex() {
        return iscomplex;
    }

    public void setIscomplex(Boolean iscomplex) {
        this.iscomplex = iscomplex;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getEditor() {
        return editor;
    }

    public void setEditor(String editor) {
        this.editor = editor;
    }

    public Integer getOrigiyear() {
        return origiyear;
    }

    public void setOrigiyear(Integer origiyear) {
        this.origiyear = origiyear;
    }

    public Integer getAuditstatus() {
        return auditstatus;
    }

    public void setAuditstatus(Integer auditstatus) {
        this.auditstatus = auditstatus;
    }

    public String getAudituser() {
        return audituser;
    }

    public void setAudituser(String audituser) {
        this.audituser = audituser;
    }

    public Date getAudittime() {
        return audittime;
    }

    public void setAudittime(Date audittime) {
        this.audittime = audittime;
    }

    public Integer getQuestionnum() {
        return questionnum;
    }

    public void setQuestionnum(Integer questionnum) {
        this.questionnum = questionnum;
    }

    public Integer getUsecount() {
        return usecount;
    }

    public void setUsecount(Integer usecount) {
        this.usecount = usecount;
    }

    public BigDecimal getCorrectrate() {
        return correctrate;
    }

    public void setCorrectrate(BigDecimal correctrate) {
        this.correctrate = correctrate;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Integer getCollectNumber() {
        return collectNumber;
    }

    public void setCollectNumber(Integer collectNumber) {
        this.collectNumber = collectNumber;
    }

    public Boolean getAutoCorrecting() {
        return autoCorrecting;
    }

    public void setAutoCorrecting(Boolean autoCorrecting) {
        this.autoCorrecting = autoCorrecting;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Integer getKnowledgeCount() {
        return knowledgeCount;
    }

    public void setKnowledgeCount(Integer knowledgeCount) {
        this.knowledgeCount = knowledgeCount;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}