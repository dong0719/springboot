package com.chendd.myspringprovider.entity;

import org.springframework.data.annotation.Id;

import java.util.List;

public class QuestionInfo {
	@Id
	private String id;
	private Question question;//题目基本信息对象
	private QuestionContent content;//题目内容
	private QuestionExtension extension;//得分率  答题次数
	private QuestionPriority priority;//题目优先级
	private QuestionUse questionUse;//题目年级使用率
	private List<QuestionItem> questionItems;//题目选项对象
	private List<QuestionKnowledgeRel> knowledges;//知识点对象
	private List<QuestionEvaluationRef> evaluations;//学科能力对象
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Question getQuestion() {
		return question;
	}
	public void setQuestion(Question question) {
		this.question = question;
	}
	public QuestionContent getContent() {
		return content;
	}
	public void setContent(QuestionContent content) {
		this.content = content;
	}
	public QuestionExtension getExtension() {
		return extension;
	}
	public void setExtension(QuestionExtension extension) {
		this.extension = extension;
	}
	public QuestionPriority getPriority() {
		return priority;
	}
	public void setPriority(QuestionPriority priority) {
		this.priority = priority;
	}
	public QuestionUse getQuestionUse() {
		return questionUse;
	}
	public void setQuestionUse(QuestionUse questionUse) {
		this.questionUse = questionUse;
	}
	public List<QuestionItem> getQuestionItems() {
		return questionItems;
	}
	public void setQuestionItems(List<QuestionItem> questionItems) {
		this.questionItems = questionItems;
	}
	public List<QuestionKnowledgeRel> getKnowledges() {
		return knowledges;
	}
	public void setKnowledges(List<QuestionKnowledgeRel> knowledges) {
		this.knowledges = knowledges;
	}
	public List<QuestionEvaluationRef> getEvaluations() {
		return evaluations;
	}
	public void setEvaluations(List<QuestionEvaluationRef> evaluations) {
		this.evaluations = evaluations;
	}
}
