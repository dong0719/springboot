package com.chendd.myspringprovider.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "question_evaluation_ref")
public class QuestionEvaluationRef {
    @Id
    private Integer eid;

    /**
     * 知识点ID
     */
    @Column(name = "question_id")
    private String questionId;

    /**
     * 学科能力ID
     */
    private Long evaluation;

    public Integer getEid() {
        return eid;
    }

    public void setEid(Integer eid) {
        this.eid = eid;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public Long getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(Long evaluation) {
        this.evaluation = evaluation;
    }
}