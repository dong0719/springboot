package com.chendd.myspringprovider.entity;

import com.yimi.yts.common.base.BasePojo;

public class QuestionPriority extends BasePojo {
    /**
     * 题目ID
     */
    private String questionId;

    /**
     * 优先级
     */
    private Double priorityNumber;

    /**
     * 同步基础正确率
     */
    private Double synchroBaseNumber;

    /**
     * 同步提高正确率
     */
    private Double synchroIncreaseNumber;

    /**
     * 同步培优正确率
     */
    private Double synchroTrainingNumber;

    /**
     * 专项提高正确率
     */
    private Double specialImprovementNumber;

    /**
     * 专项培优正确率
     */
    private Double specialTrainingNumber;

    /**
     * 题目标签（1.经典题2.精品题3.常考题4.普通题）
     */
    private Double questionTypeNumber;

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId == null ? null : questionId.trim();
    }

    public Double getPriorityNumber() {
        return priorityNumber;
    }

    public void setPriorityNumber(Double priorityNumber) {
        this.priorityNumber = priorityNumber;
    }

    public Double getSynchroBaseNumber() {
        return synchroBaseNumber;
    }

    public void setSynchroBaseNumber(Double synchroBaseNumber) {
        this.synchroBaseNumber = synchroBaseNumber;
    }

    public Double getSynchroIncreaseNumber() {
        return synchroIncreaseNumber;
    }

    public void setSynchroIncreaseNumber(Double synchroIncreaseNumber) {
        this.synchroIncreaseNumber = synchroIncreaseNumber;
    }

    public Double getSynchroTrainingNumber() {
        return synchroTrainingNumber;
    }

    public void setSynchroTrainingNumber(Double synchroTrainingNumber) {
        this.synchroTrainingNumber = synchroTrainingNumber;
    }

    public Double getSpecialImprovementNumber() {
        return specialImprovementNumber;
    }

    public void setSpecialImprovementNumber(Double specialImprovementNumber) {
        this.specialImprovementNumber = specialImprovementNumber;
    }

    public Double getSpecialTrainingNumber() {
        return specialTrainingNumber;
    }

    public void setSpecialTrainingNumber(Double specialTrainingNumber) {
        this.specialTrainingNumber = specialTrainingNumber;
    }

    public Double getQuestionTypeNumber() {
        return questionTypeNumber;
    }

    public void setQuestionTypeNumber(Double questionTypeNumber) {
        this.questionTypeNumber = questionTypeNumber;
    }
}