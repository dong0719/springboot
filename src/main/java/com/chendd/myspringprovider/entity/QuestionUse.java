package com.chendd.myspringprovider.entity;

import com.yimi.yts.common.base.BasePojo;

public class QuestionUse extends BasePojo {
    /**
     *
     */
    private Integer id;

    /**
     * 题目ID
     */
    private String questionId;

    /**
     * 年级ID
     */
    private String gradeId;

    /**
     * 使用次数
     */
    private Integer useCount;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId == null ? null : questionId.trim();
    }

    public String getGradeId() {
        return gradeId;
    }

    public void setGradeId(String gradeId) {
        this.gradeId = gradeId == null ? null : gradeId.trim();
    }

    public Integer getUseCount() {
        return useCount;
    }

    public void setUseCount(Integer useCount) {
        this.useCount = useCount;
    }
}