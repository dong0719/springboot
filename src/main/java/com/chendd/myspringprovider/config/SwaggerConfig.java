package com.chendd.myspringprovider.config;

import com.google.common.base.Predicates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.async.DeferredResult;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Description:
 *
 * @author DongDong.Chen
 * @date 2017-11-23 14:25
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @SuppressWarnings("unchecked")
    @Bean
    public Docket questionApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("")
                .genericModelSubstitutes(DeferredResult.class)
                .genericModelSubstitutes(ResponseEntity.class)
                .useDefaultResponseMessages(false)
                .forCodeGeneration(false)
                .pathMapping("/")
                .select()
                //过滤的接口
                .paths(Predicates.or(PathSelectors.regex("/.*")))
                .build()
                .apiInfo(questionApiInfo());
    }


    private ApiInfo questionApiInfo() {
        ApiInfo apiInfo = new ApiInfo(
                //大标题
                "相关接口",
                //小标题
                "注意：有任何问题请及时联系:dongdong.chen@1mifudao.com",
                //版本
                "1.0",
                //服务条款
                "",
                //作者
                "DongDong.Chen",
                //链接显示文字
                "一米辅导",
                //网站链接
                "http://www.1mifudao.com"
        );
        return apiInfo;
    }
}
