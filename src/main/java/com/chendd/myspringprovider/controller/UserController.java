package com.chendd.myspringprovider.controller;

import com.chendd.myspringprovider.dao.QuestionDaoImpl;
import com.chendd.myspringprovider.entity.QuestionInfo;
import com.chendd.myspringprovider.entity.User;
import com.chendd.myspringprovider.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Description:
 *
 * @author DongDong.Chen
 * @date 2017-11-22 15:03
 */
@RestController
public class UserController {
    @Autowired
    UserMapper userMapper;

    @Autowired
    QuestionDaoImpl questionDao;

    @PostMapping("/getAllUser")
    public List<User> getAllUser(){
        return userMapper.selectAll();
    }

    @PostMapping("/insert")
    public int insert(@RequestBody User user){
        return userMapper.insert(user);
    }

    @PostMapping("/update")
    public int update(@RequestBody User user){
        return userMapper.updateByPrimaryKeySelective(user);
    }

    @PostMapping("/delete")
    public int delete(@RequestBody User user){
        return userMapper.delete(user);
    }

    @PostMapping("/getAll")
    public List<QuestionInfo> getAll(){
        return questionDao.queryQuestion();
    }
}