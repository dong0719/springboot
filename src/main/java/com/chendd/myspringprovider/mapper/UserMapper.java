package com.chendd.myspringprovider.mapper;

import com.chendd.myspringprovider.entity.User;
import tk.mybatis.mapper.common.Mapper;

/**
 * Description:
 *
 * @author DongDong.Chen
 * @date 2017-11-22 17:44
 */
public interface UserMapper extends Mapper<User> {
}
