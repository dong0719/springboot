package com.chendd.myspringprovider;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.chendd.myspringprovider.mapper")
public class MyspringProviderApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyspringProviderApplication.class, args);
	}
}
